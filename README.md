# chwp

Changes the background wallpaper (and lockscreen) from the command line.

## Build
build dependencies:
 - build-essential
 - cmake 
 - qt5-base (debian/ubuntu: qtbase5-dev)

execute: `./build.sh`   
  
## Installation
install dependencies:
 - qt5-base (Qt5Core, Qt5Network, Qt5Gui; debian/ubuntu: libqt5core5a, libqt5gui5)
 - xorg-xrandr

### Arch Linux
AUR: https://aur.archlinux.org/packages/chwp-git

AUR: `yay -S chwp-git`

### Other

execute: `./install.sh` 

## Usage

The following examples shows how to use the chwp command line tool to change the wallpaper.

#### Changes the wallpaper and lockscreen to a random image from unsplash
```bash
chwp
```

#### Changes the wallpaper according to the given keywords
```bash
chwp water,nature
```

#### Spans one wallpaper over all monitors
```bash
chwp water,nature span
```

#### Changes the wallpaper by a given image url
```bash
chwp https://source.unsplash.com/daily
```
```bash
chwp https://upload.wikimedia.org/wikipedia/commons/e/e1/FullMoon2010.jpg
```

#### Select a random wallpaper from all specified wallpaper arguments.
```bash
chwp nasa,galaxy /home/user/Pictures/wallpaper https://url/to/image.jpg
```