#include <QtNetwork>
#include <QtCore>

class FileDownloader : public QObject {
Q_OBJECT

public:

    static QString getStringResponse(const QString &url);

    void download(const QUrl &url, const QString &targetFile);

signals:

    void downloaded(QString string);

private slots:

    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

    void downloadFinished();

    void downloadReadyRead();

private:
    QNetworkReply *networkReply = nullptr;
    QFile output;
    QElapsedTimer downloadTime;
};