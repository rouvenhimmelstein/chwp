#include <QtCore>

static const char *const unsplashClientId = "03477508efcc77876bf4285462d463358a55a7da6416a3dc58ad841cfce2fb86";

class WallpaperChanger : public QObject {
Q_OBJECT

public:

    void initialize();

    QUrl getWallpaperUrl();

    static int getRandomInt(int min, int max);

    static QDir getWallpaperBaseDir();

    static QString getWallpaperFile();

    static QString getWallpaperTempFile();

signals:

    void wallpaperChanged();

public slots:

    void setWallpaper(const QString &tempFileName);

private:
    QStringList args;
    bool span;
    // landscape, portrait, and squarish.
    QString orientation;
    QString maxSingleDisplayResolution;
    QString spannedDisplayResolution;
    QString displayResolution;
    float displayRatio;

    static QString executeCommand(const QString &command);

    static QString detectDesktop();

    static QString createKdePlasmaWallpaperCommand(const QString &fileName);

    static QString createKdePlasmaLockscreenCommand(const QString &fileName);

    static void setXfceWallpaper(const QString &fileName);

    static int sumResolution(const QString &resoString);

    static QString getMaxSingleDisplayResolution();

    static QString getSpannedDisplayResolution();

    QString getUnsplashUrl(const QString &keywords);

    QUrl getRandomUrl(const QString &keywords, const QStringList &folders, const QStringList &urls, bool span);

    static QUrl loadRandomLocalImage(QString folderPath);

    static void rescaleImage(const QString &fileName, const QString &displayResolution, float displayRatio);

    static float getDisplayRation(const QString &resolution);

    static float getImageRatio(const QString &imageFilePath);

    static QString readGSettings(const QString &key);

    static void writeGSettings(const QString &key, const QString &value);

    static bool isGSettingsValueEquals(const QString &key, const QString &value);

    static bool isDisplayVarSet();

    static QString getOrientation(float ratio);

    QString getFirstFittingResponse(const QJsonArray &array);

    bool fitsToDisplayResolution(const QJsonObject &imageObject);

    static QString getRandomKeyword(const QString &keywords);
};
