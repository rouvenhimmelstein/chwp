#include "filedownloader.h"
#include "wallpaperchanger.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
#ifdef NDEBUG
#else
        case QtDebugMsg:
            fprintf(stdout, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;
#endif
        case QtInfoMsg:
            fprintf(stdout, "%s\n", localMsg.constData());
            //fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
            break;

        case QtWarningMsg:
            fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;

        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            abort();
    }
}

int main(int argc, char *argv[]) {
    qInstallMessageHandler(myMessageOutput);

    QCoreApplication app(argc, argv);

    QString wallpaperTempFile = WallpaperChanger::getWallpaperTempFile();

    WallpaperChanger wallpaperChanger;
    FileDownloader fileDownloader;

    wallpaperChanger.initialize();

    QObject::connect(&fileDownloader, SIGNAL(downloaded(QString)), &wallpaperChanger, SLOT(setWallpaper(QString)));
    QObject::connect(&wallpaperChanger, SIGNAL(wallpaperChanged()), &app, SLOT(quit()));

    QUrl wallpaperUrl = wallpaperChanger.getWallpaperUrl();
    qDebug() << "Wallpaper url: " << wallpaperUrl.toString();

    fileDownloader.download(wallpaperUrl, wallpaperTempFile);

    return QCoreApplication::exec();
}
