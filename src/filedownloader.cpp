#include "filedownloader.h"

void FileDownloader::downloadProgress(qint64 bytesReceived, qint64 bytesTotal) {
    // calculate the download speed
    double speed = bytesReceived * 1000.0 / downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024 * 1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024 * 1024;
        unit = "MB/s";
    }

    const QString &qString = QString::fromLatin1("%1 %2")
            .arg(speed, 3, 'f', 1).arg(unit);

    qDebug() << qString;
}

void FileDownloader::downloadFinished() {
    output.close();

    if (networkReply->error()) {
        fprintf(stderr, "Failed: %s\n", qPrintable(networkReply->errorString()));
        output.remove();
    } else {
        qDebug() << "Succeeded.";
    }

    QString string = output.fileName();
    emit downloaded(string);
    networkReply->deleteLater();
}

void FileDownloader::downloadReadyRead() {
    output.write(networkReply->readAll());
}

void FileDownloader::download(const QUrl &url, const QString &targetFile) {
    auto *pManager = new QNetworkAccessManager(QCoreApplication::instance());
    pManager->setRedirectPolicy(QNetworkRequest::RedirectPolicy::NoLessSafeRedirectPolicy);

    output.setFileName(targetFile);
    if (!output.open(QIODevice::WriteOnly)) {
        fprintf(stderr, "Problem opening save file '%s' for download '%s': %s\n",
                qPrintable(targetFile), url.toEncoded().constData(),
                qPrintable(output.errorString()));

        return;
    }

    QNetworkRequest request(url);

    networkReply = pManager->get(request);
    connect(networkReply, SIGNAL(downloadProgress(qint64, qint64)), SLOT(downloadProgress(qint64, qint64)));
    connect(networkReply, SIGNAL(finished()), SLOT(downloadFinished()));
    connect(networkReply, SIGNAL(readyRead()), SLOT(downloadReadyRead()));

    downloadTime.start();
}

QString FileDownloader::getStringResponse(const QString &urlString) {
    QNetworkAccessManager networkManager;
    QNetworkRequest request;
    request.setUrl(QUrl(urlString));
    QNetworkReply *reply = networkManager.get(request);

    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), &loop, SLOT(quit()));
    loop.exec();

    QByteArray bts = reply->readAll();
    QString stringResponse(bts);

    qDebug() << stringResponse;

    return stringResponse;
}

