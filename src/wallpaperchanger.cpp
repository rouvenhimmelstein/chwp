#include <QtGui/QImage>
#include <QtGui/QImageReader>
#include "wallpaperchanger.h"
#include "filedownloader.h"

void WallpaperChanger::initialize() {
    args = QCoreApplication::arguments();
    if (!args.isEmpty() && args[0].endsWith("chwp")) {
        args.removeAt(0);
    }
    if (args.contains("span")) {
        span = true;
        args.removeAll("span");
    } else {
        span = false;
    }

    maxSingleDisplayResolution = getMaxSingleDisplayResolution();
    spannedDisplayResolution = getSpannedDisplayResolution();
    displayResolution = span ? spannedDisplayResolution : maxSingleDisplayResolution;
    displayRatio = span ? getDisplayRation(spannedDisplayResolution) : getDisplayRation(maxSingleDisplayResolution);
    orientation = getOrientation(displayRatio);

    qDebug() << "Spanning: " << span;
    qDebug() << "Max resolution: " << maxSingleDisplayResolution;
    qDebug() << "spanned resolution: " << spannedDisplayResolution;
    qDebug() << "Selected resolution: " << displayResolution;
    qDebug() << "Display ratio: " << displayRatio;
    qDebug() << "Display orientation: " << orientation;
}

QDir WallpaperChanger::getWallpaperBaseDir() {
    QDir homeDir = QDir::home();
    homeDir.mkdir(".wallpaper");
    homeDir.cd(".wallpaper");
    return homeDir;
}

QString WallpaperChanger::getWallpaperFile() {
    return getWallpaperBaseDir()
            .absoluteFilePath("wallpaper.jpeg");
}

QString WallpaperChanger::getWallpaperTempFile() {
    return getWallpaperBaseDir()
            .absoluteFilePath("temp.jpeg");
}

int WallpaperChanger::getRandomInt(int min, int max) {
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(min, max);
    return distr(eng);
}

QUrl WallpaperChanger::getWallpaperUrl() {
    QString keywords;
    QStringList folders;
    QStringList urls;

    if (args.isEmpty()) {
        return QUrl(getUnsplashUrl("wallpaper"));
    } else {

        urls = args.filter(QRegExp("http(s)?:\\/\\/.*"));
        for (const auto &url : urls) {
            args.removeAll(url);
        }

        folders = args.filter(QRegExp(R"((\/.*|\"\/.*\"))"));
        for (const auto &folder : folders) {
            args.removeAll(folder);
        }

        if (!args.isEmpty()) {
            keywords = args.first();
        }

        return getRandomUrl(keywords, folders, urls, span);
    }
}

QUrl WallpaperChanger::getRandomUrl(const QString &keywords, const QStringList &folders, const QStringList &urls,
                                    bool span) {
    QStringList patterns;

    if (!keywords.isEmpty()) {
        patterns << "keywords";
    }
    if (!folders.isEmpty()) {
        patterns << "folders";
    }
    if (!urls.isEmpty()) {
        patterns << "urls";
    }

    int randomIndex = getRandomInt(0, patterns.size() - 1);

    QUrl wallpaperUrl;
    QString pattern = patterns[randomIndex];

    if (pattern == "keywords") {
        wallpaperUrl = QUrl(getUnsplashUrl(keywords));
    }
    if (pattern == "folders") {
        wallpaperUrl = loadRandomLocalImage(folders[getRandomInt(0, folders.size() - 1)]);
    }
    if (pattern == "urls") {
        wallpaperUrl = QUrl(urls[getRandomInt(0, urls.size() - 1)]);
    }

    return wallpaperUrl;
}

QString WallpaperChanger::getMaxSingleDisplayResolution() {
    QString getResolutionCmd = "xrandr | grep \\* | cut -d' ' -f4";
    QString resolutionsString;

    if (isDisplayVarSet()) {
        resolutionsString = executeCommand(getResolutionCmd);
    } else {
        resolutionsString = executeCommand("DISPLAY=:0 " + getResolutionCmd);
    }

    if (resolutionsString.contains("\n")) {
        const QStringList &resolutions = resolutionsString.trimmed().split("\n");
        resolutionsString = resolutions[0];
        int resoInt = sumResolution(resolutionsString);
        for (const auto &resolution : resolutions) {
            int iInt = sumResolution(resolution);
            if (iInt > resoInt) {
                resolutionsString = resolution;
            }
        }
    }

    return resolutionsString.trimmed();
}

bool WallpaperChanger::isDisplayVarSet() {
    return !executeCommand("echo $DISPLAY").trimmed().isEmpty();
}

QString WallpaperChanger::executeCommand(const QString &command) {
    QProcess process;
    process.start("bash", QStringList() << "-c" << command);
    process.waitForFinished(-1); // will wait forever until finished

    QString stdout = process.readAllStandardOutput();
    QString stderr = process.readAllStandardError();
    if (!stderr.isEmpty()) {
        qDebug() << "Command: " << command << "produces error: " << stderr;
    }
    return stdout;
}

QString WallpaperChanger::readGSettings(const QString &key) {
    return executeCommand("gsettings get " + key);
}

void WallpaperChanger::writeGSettings(const QString &key, const QString &value) {
    if (!isGSettingsValueEquals(key, value)) {
        executeCommand("gsettings set " + key + " " + value);
    }
}

bool WallpaperChanger::isGSettingsValueEquals(const QString &key, const QString &value) {
    return readGSettings(key) == value;
}

void WallpaperChanger::setWallpaper(const QString &tempFileName) {
    QString desktop = detectDesktop();
    float imageRatio = getImageRatio(tempFileName);

    if (imageRatio != displayRatio) {
        rescaleImage(tempFileName, displayResolution, displayRatio);
    }

    QString wallpaperFileName = getWallpaperFile();
    QFile wallpaperFile(wallpaperFileName);
    QFile tempFile(tempFileName);

    wallpaperFile.remove();
    tempFile.rename(wallpaperFileName);

    QString pictureOption;

    if (span) {
        pictureOption = "spanned";
    } else {
        pictureOption = "scaled";
    }

    if (desktop.contains("gnome")) {
        writeGSettings("org.gnome.desktop.background picture-uri", "file://" + wallpaperFileName);
        writeGSettings("org.gnome.desktop.background picture-options", pictureOption);
        writeGSettings("org.gnome.desktop.screensaver picture-uri", "file://" + wallpaperFileName);
        writeGSettings("org.gnome.desktop.screensaver picture-options", pictureOption);
    } else if (desktop.contains("cinnamon")) {
        writeGSettings("org.cinnamon.desktop.background picture-uri", "file://" + wallpaperFileName);
        writeGSettings("org.cinnamon.desktop.background picture-options", pictureOption);
    } else if (desktop.contains("deepin")) {
        writeGSettings("com.deepin.wrap.gnome.desktop.background picture-uri", "file://" + wallpaperFileName);
        writeGSettings("com.deepin.wrap.gnome.desktop.background picture-options", pictureOption);
        writeGSettings("com.deepin.wrap.gnome.desktop.screensaver picture-uri", "file://" + wallpaperFileName);
        writeGSettings("com.deepin.wrap.gnome.desktop.screensaver picture-options", pictureOption);
    } else if (desktop.contains("plasma") || desktop.contains("kde")) {
        executeCommand(createKdePlasmaWallpaperCommand(""));
        executeCommand(createKdePlasmaWallpaperCommand(wallpaperFileName));
        executeCommand(createKdePlasmaLockscreenCommand(wallpaperFileName));
    } else if (desktop.contains("xfce")) {
        if (QFile::exists("/bin/feh") || QFile::exists("usr/bin/feh")) {
            executeCommand("feh --bg-scale " + wallpaperFileName);
        } else {
            setXfceWallpaper(wallpaperFileName);
        }
    } else {
        qInfo() << desktop << " is not supported yet.";
    }

    emit wallpaperChanged();
}

QString WallpaperChanger::detectDesktop() {
    return QString(std::getenv("XDG_CURRENT_DESKTOP")).toLower();
}

QString WallpaperChanger::createKdePlasmaWallpaperCommand(const QString &fileName) {
    QString kdeCommand = QString(
            "dbus-send --session --dest=org.kde.plasmashell --type=method_call /PlasmaShell org.kde.PlasmaShell.evaluateScript 'string: "
            "var Desktops = desktops();        "
            "for (i=0;i<Desktops.length;i++) { "
            "        d = Desktops[i]; "
            "        d.wallpaperPlugin = \"org.kde.image\"; "
            "        d.currentConfigGroup = Array(\"Wallpaper\", "
            "                                    \"org.kde.image\", "
            "                                    \"General\"); "
            "        d.writeConfig(\"Image\", \"file://" + fileName + "\"); "
                                                                      "}'");
    return kdeCommand;
}

QString WallpaperChanger::createKdePlasmaLockscreenCommand(const QString &fileName) {
    QString kdeCommand = QString("kwriteconfig5 --file ~/.config/kscreenlockerrc "
                                 "--group Greeter "
                                 "--group Wallpaper "
                                 "--group org.kde.image "
                                 "--group General "
                                 "--key Image 'file://" + fileName + "'");
    return kdeCommand;
}

QString WallpaperChanger::getSpannedDisplayResolution() {
    QString cmd = R"(xrandr -q|sed -n 's/.*current[ ]\([0-9]*\) x \([0-9]*\),.*/\1x\2/p')";
    QString cmd2 = R"(DISPLAY=:0 xrandr -q|sed -n 's/.*current[ ]\([0-9]*\) x \([0-9]*\),.*/\1x\2/p')";

    if (isDisplayVarSet()) {
        return executeCommand(cmd).trimmed();
    } else {
        return executeCommand(cmd2).trimmed();
    }
}

void WallpaperChanger::setXfceWallpaper(const QString &fileName) {
    const QString &channels = executeCommand("xfconf-query -c xfce4-desktop -l | grep \"last-image$\"");
    const QStringList &channelList = channels.split("\n");

    for (const QString &channel : channelList) {
        executeCommand("xfconf-query --channel xfce4-desktop --property " + channel + " --set " + fileName);
    }
}

int WallpaperChanger::sumResolution(const QString &resoString) {
    const QStringList &list = resoString.split("x");
    int a = list[0].toInt();
    int b = list[1].toInt();
    return a + b;
}

QString WallpaperChanger::getUnsplashUrl(const QString &keywords) {
    QString randomKeyword = getRandomKeyword(keywords);

    QString requestUrl = "https://api.unsplash.com/photos/random"
                         "?query=" + randomKeyword +
                         "&count=25" +
                         "&orientation=" + orientation +
                         "&client_id=" + unsplashClientId;

    qDebug() << "request uri: " << requestUrl;

    QString stringResponse = FileDownloader::getStringResponse(requestUrl);
    QJsonArray responseArray = QJsonDocument::fromJson(stringResponse.toUtf8()).array();
    return getFirstFittingResponse(responseArray);
}

QUrl WallpaperChanger::loadRandomLocalImage(QString folderPath) {
    QUrl url;
    folderPath.replace("\"", "");

    QStringList filter;
    filter << QLatin1String("*.png");
    filter << QLatin1String("*.jpeg");
    filter << QLatin1String("*.jpg");
    filter << QLatin1String("*.bmp");

    QDir dir(folderPath);
    dir.setNameFilters(filter);
    QFileInfoList filelistinfo = dir.entryInfoList();

    if (!filelistinfo.isEmpty()) {
        QFileInfo &info = filelistinfo[getRandomInt(0, filelistinfo.size() - 1)];
        url = QUrl::fromLocalFile(info.absoluteFilePath());
    }

    return url;
}

void WallpaperChanger::rescaleImage(const QString &fileName, const QString &displayResolution, float displayRatio) {
    QImage image(fileName);
    int imgWidth = image.width();
    int imgHeight = image.height();
    float imgRatio = (float) imgWidth / (float) imgHeight;

    int newImageWidth;
    int newImageHeight;
    int xStart = 0;
    int yStart = 0;

    if (imgRatio == displayRatio) {
        qDebug() << "No rescaling of image, display & image ratios are equal: " << imgRatio;
        return;
    } else if (imgRatio < displayRatio) {
        newImageWidth = imgWidth;
        newImageHeight = imgWidth / displayRatio;

        yStart = (imgHeight / 2) - (newImageHeight / 2);
    } else {
        newImageWidth = imgHeight * displayRatio;
        newImageHeight = imgHeight;

        xStart = (imgWidth / 2) - (newImageWidth / 2);
    }
    float newImageRatio = (float) newImageWidth / (float) newImageHeight;

    QImage scaledImage = image.copy(xStart, yStart, newImageWidth, newImageHeight);
    scaledImage.save(fileName);

    qDebug() << "Scaled image from "
             << imgWidth << "x" << imgHeight
             << " to "
             << newImageWidth << "x" << newImageHeight << " ratio: " << newImageRatio
             << " display ratio: " << displayRatio;
}

float WallpaperChanger::getDisplayRation(const QString &resolution) {
    const QStringList &list = resolution.split("x");
    float width = list[0].toFloat();
    float height = list[1].toFloat();
    return width / height;
}

float WallpaperChanger::getImageRatio(const QString &imageFilePath) {
    QImageReader reader(imageFilePath);
    QSize sizeOfImage = reader.size();
    int height = sizeOfImage.height();
    int width = sizeOfImage.width();

    return width / height;
}

QString WallpaperChanger::getOrientation(float ratio) {
    if (ratio == 1.f) { return "squarish"; }
    else if (ratio < 1.f) { return "portrait"; }
    else if (ratio > 1.f) { return "landscape"; }
    return "";
}

QString WallpaperChanger::getFirstFittingResponse(const QJsonArray &jsonArray) {
    for (auto &&element : jsonArray) {
        QJsonObject imageObject = element.toObject();

        if (fitsToDisplayResolution(imageObject)) {
            QJsonObject user = imageObject["user"].toObject();
//            qInfo() << "Owner of the photo is: " << user["name"].toString() << " ("
//                    << user["links"].toObject()["html"].toString() << ")";
//            qInfo() << "Direct link to the photo: " << imageObject["links"].toObject()["html"].toString();
            return imageObject["urls"].toObject()["raw"].toString();
        }
    }

    // if no image is big enough for current display resolution, just take the first.
    return jsonArray[0].toObject()["urls"].toObject()["raw"].toString();
}

bool WallpaperChanger::fitsToDisplayResolution(const QJsonObject &imageObject) {
    const QStringList &list = displayResolution.split("x");
    int displayWidth = list[0].toInt();
    int displayHeight = list[1].toInt();

    int imageWidth = imageObject["width"].toInt();
    int imageHeight = imageObject["height"].toInt();

    bool displayFitsInto = imageWidth >= displayWidth && imageHeight >= displayHeight;

    qDebug() << "Image Width: " << imageWidth;
    qDebug() << "Image Height: " << imageHeight;
    qDebug() << "display Width: " << displayWidth;
    qDebug() << "display Height: " << displayHeight;
    qDebug() << "Display fits into image : " << displayFitsInto;

    return displayFitsInto;
}

QString WallpaperChanger::getRandomKeyword(const QString &keywords) {
    QString keyword = keywords;

    if (keywords.contains(",")) {
        const QStringList &keywordList = keywords.split(",");
        int randomIndex = getRandomInt(0, keywordList.size() - 1);
        keyword = keywordList[randomIndex];
    }

    qDebug() << "unsplash random keyword: " << keyword;

    return keyword;
}
