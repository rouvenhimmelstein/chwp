#!/bin/sh

# prepare
mkdir -p cmake-build-release

# build
cd cmake-build-release
cmake .. -DCMAKE_BUILD_TYPE=Release
make
